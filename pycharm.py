from flask import Flask, jsonify,request
import mysql.connector

app = Flask(__name__)

@app.route("/",methods =["GET"])
def root():
    return '<h1 style = "color: red">WEATHER REPORTING SYSTEM USING IOT</h1>'

@app.route("/values", methods=["POST"])
def post_values():
    temperature = request.json.get("temp")
    humidity = request.json.get("humid")
    connection = mysql.connector.connect(host="localhost", user="root", password="root", database="project_iot")
    cursor = connection.cursor()
    statement = f"insert into project (temperature_value,humidity_value) values ({temperature},{humidity})"
    cursor.execute(statement)
    connection.commit()
    cursor.close()
    connection.close()
    return "inserted new values"

@app.route("/values", methods=["GET"])
def get_values():
    connection = mysql.connector.connect(host="localhost", user="root", password="root", database="project_iot")
    cursor = connection.cursor()
    statement = "select id,temperature_value,humidity_value,time from project"
    cursor.execute(statement)
    data = cursor.fetchall()
    project = []
    for (id,temperature_value,humidity_value,time) in data:
        values={
            "id": id,
            "temperature_value": temperature_value,
            "humidity_value": humidity_value,
            "time": time
        }
        project.append(values)
    cursor.close()
    connection.close()
    return jsonify(project)

if __name__ == '__main__':
    app.run(port=4000,host="192.168.43.47",debug="true")