#include<WiFiClient.h>
#include<ESP8266WiFi.h>
#include<ESP8266HTTPClient.h>

#include "DHT.h"        
#define DHTTYPE DHT11   
#define dht_dpin 0



DHT dht(dht_dpin, DHTTYPE); 

const char *ssid ="Yash";
char const *password = "yash1234";

void setup() {

Serial.flush();
Serial.begin(115200);
dht.begin();

pinMode(D7,OUTPUT);
pinMode(D4,OUTPUT);

WiFi.mode(WIFI_STA);
WiFi.begin(ssid,password);
  
Serial.print("Connecting.");
while(WiFi.status()!=WL_CONNECTED)
{
  delay(500);
  Serial.print(".");
}

 Serial.println("Connected...");
 Serial.print("IP Address:");
 Serial.println(WiFi.localIP());
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);
  
  // Reading temperature or humidity takes about 250 milliseconds! 
  float h = dht.readHumidity();
  
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  if( t >= 30.00){
     
   
       digitalWrite(D7,HIGH);

    }else{
      digitalWrite(D7,LOW);
    }

    
  if( t <= 25.00){
     
   
       digitalWrite(D4,HIGH);

    }else{
      digitalWrite(D4,LOW);
    }

  String body = "{ \"temp\":" + String(t) + "," + "\"humid\":" + String(h)+ "}";
  Serial.println("sending : "+body);
  
  HTTPClient httpClient;

  httpClient.begin("http://192.168.43.47:4000/values");

  httpClient.addHeader("Content-Type", "application/json");

  int statusCode = httpClient.POST(body);

  Serial.println("status code: "+ String(statusCode));
  httpClient.end();

  delay(2000);

}
